# Breezily Autokey

Breezily makes Ubuntu productivity a breeze.

How? By adding efficient keyboard shortcuts so you can navigate your desktop with ease.

The [Autokey](https://github.com/autokey/autokey) program is used for implementing Breezily's keyboard shortcut scripts.

The xkb symbols file (located in `/usr/share/X11/xkb/symbols/pc`) is modified so we can remap `Caps Lock` to the `Hyper` key.

Breezily's Autokey scripts use the `Hyper` key to execute these useful keyboard shortcuts.

Also used are [xbindkeys](https://manpages.ubuntu.com/manpages/bionic/man1/xbindkeys.1.html) and [xdotool](https://manpages.ubuntu.com/manpages/impish/man1/xdotool.1.html) for creating mouse cursor movements with the keyboard (optional).

## Getting Autokey Up and Running

1. Install `Autokey` via terminal with `sudo apt install autokey-gtk` or `sudo apt install autokey-qt`
2. Next, launch the `Autokey` program (it should appear in your applications list) - this will create the `~/.config/Autokey/data` folder.
3. Open terminal and clone the `breezily-autokey/` repo into your `~/Downloads` folder.
4. `cd` into `~/Downloads/breezily-autokey/`, then run `sh ./install.sh` to start the Breezily installer.
5. Inside the Breezily installer menu, choose option 1.
6. Log out and log back in for the changes to take effect.

## Installing the Mouse Pointer Movement Feature (optional)

1. Open terminal and clone the `breezily-autokey/` repo into your `~/Downloads` folder (if you haven't already).
2. `cd` into `~/Downloads/breezily-autokey/`, then run `sh ./install.sh` to start the Breezily installer.
3. Inside the Breezily installer menu, choose option 2.
4. If `xbindkeys` does not startup automatically on login, edit your [system startup setttings](https://linuxhint.com/run_linux_command_script_sys_reboot/) and add the `xbindkeys` command to the list of startup programs.
5. Log out and log back in for the changes to take effect.

- The Breezily `.xbindkeysrc` file uses the `xrandr` command to dynamically create mouse cursor movement pixel values based on your screen's dimensions. These values are then stored in temporary variables.

- The `xdotool` program uses these variable values within the `.xbindkeysrc` file to help move your mouse cursor (when you enter a Breezily command to move the mouse cursor, an `xdotool` command is also executed).

- After following the instructions below to setup, you will be able to move your mouse cursor with your keyboard. Using `Alt+W` for instance moves your mouse cursor up, `Alt+S` moves your cursor down, `Alt+A` moves the cursor left, etc.

- In addition, keyboard shortcuts will be added so you can snap windows into four screen quadrants (i.e. upper-left quadrant, upper-right quadrant, lower-left quadrant, etc.)

## Uninstalling Breezily

1. `cd` into the `breezily-autokey/` repo.
2. Run `sh uninstall.sh` to launch the uninstall script.
3. Choose option 1 to remove all Breezily Autokey scripts and restore the Caps_Lock key to its original state.
4. Choose option 2 to remove the `xbindkeysrc` and `xdotool` packages and delete the `.xbindkeysrc` file from your home `~/` directory.
5. Choose option 3 to execute options 1 & 2.

# Breezily Keyboard Shortcuts

Please note that the shortcut behvaior could vary depending on the app in use.

## Essential Shortcuts

- **Caps Lock-Z**: Undo the previous command. You can then press **Caps Lock-X** to Redo, reversing the undo command. In some apps, you can undo and redo multiple commands.
- **Caps Lock-C**: Copy the selected item to the Clipboard. This also works for files in the File Explorer.
- **Caps Lock-I**: Cut the selected item and copy it to the Clipboard.
- **Caps Lock-V**: Paste the contents of the Clipboard into the current document/app. This also works for files in the File Explorer.
- **Caps Lock-G**: Find items in a document or open the Find tool.
- **Caps Lock-S**: Save the current document/item.
- **Shift-Space**: Rename a selected file/item.
- **Caps Lock-Space**: Press the `Super` key.
- **Caps Lock-Q**: Press the `Escape` key.
- **Caps Lock-Shift-C**: Press the `Control-Shift-C` keys together.
- **Caps Lock-Shift-F**: Press the `Control-Shift-F` keys together.
- **Caps Lock-Shift-N**: Press the `Control-Shift-N` keys together.
- **Caps Lock-Shift-O**: Press the `Control-Shift-O` keys together.
- **Caps Lock-Shift-S**: Press the `Control-Shift-S` keys together.
- **Caps Lock-Shift-T**: Press the `Control-Shift-T` keys together.
- **Caps Lock-Shift-Tab**: Press the `Control-Shift-Tab` keys together.
- **Caps Lock-Alt-Tab**: Press the `Control-Shift-Tab` keys together.
- **Caps Lock-Shift-V**: Press the `Control-Shift-V` keys together.
- **Caps Lock-Shift-W**: Press the `Control-Shift-W` keys together.
- **Caps Lock-9**: Insert a unicode bullet character.
- **Caps Lock-Enter**: Press the `Control-Enter` keys together.

## Document Shortcuts

- **Caps Lock-D**: Delete the character to the right of the insertion point.
- **Caps Lock-H**: Delete the character to the left of the insertion point.
- **Caps Lock-F**: Move one character forward.
- **Caps Lock-B**: Move one character backward.
- **Caps Lock-P**: Move up one line.
- **Caps Lock-N**: Move down one line.
- **Caps Lock-O**: Scroll up on the current page.
- **Caps Lock-L**: Scroll down on the current page.
- **Caps Lock-Y**: Highlight/select all items.
- **Caps Lock-A**: Move to the beginning of the line/paragraph.
- **Caps Lock-E**:  Move to the end of the line/paragraph.
- **Caps Lock-K**: Delete text between the insertion point and the end of line/paragraph.
- **Caps Lock-Semicolon (;)**: Extend text selection one character to the left.
- **Caps Lock-Apostrophe (')**: Extend text selection one character to the right.
- **Caps Lock-Left Curly Brace ({)**: Extend text selection to beginning of the current word, then to the beginning of the following word if pressed again.
- **Caps Lock-Right Curly Brace (})**: Extend text selection to end of the current word, then to the end of the following word if pressed again.
- **Caps Lock-Backspace**: Select the text between the insertion point and the beginning of the current line.
- **Caps Lock-Backslash (\\)**: Select the text between the insertion point and the end of the current line.
- **Caps Lock-U**: Extend text selection to the nearest character at the same horizontal location on the line above.
- **Caps Lock-J**: Extend text selection to the nearest character at the same horizontal location on the line below.

## Window Shortcuts

- **Caps Lock-4**: Open a new window.
- **Caps Lock-W**: Close the active window.

## Web Browser Shortcuts

- **Caps Lock-R**: Reload the current page.
- **Caps Lock-T**: Open a new tab, and move to it.
- **Caps Lock-Tab**: Move to the next open tab.
- **Caps Lock-Shift-Tab**: Move to the previous open tab
- **Caps Lock-Backtick (`)**: Reopen the last closed tab, and move to it.
- **Caps Lock-1**: Open the previous page from your browsing history in the current tab.
- **Caps Lock-2**: Open the next page from your browsing history in the current tab.
- **Caps Lock-3**: Move to the address bar.
- **Caps Lock-0**: Return contents on the page to its default size.
- **Caps Lock-Minus sign (-)**: Decrease the size of the current page.
- **Caps Lock-Plus sign (+)**: Increase the size of the current page.

## Mouse Cursor Shortcuts (Optional)

- **Alt-C**: Click the left mouse button.
- **Alt-V**: Click the right mouse button.
- **Alt-W**: Move the mouse cursor up.
- **Alt-A**: Move the mouse cursor left.
- **Alt-S**: Move the mouse cursor down.
- **Alt-D**: Move the mouse cursor right.
- **Alt-Shift-W**: Move the mouse cursor up at double the distance.
- **Alt-Shift-A**: Move the mouse cursor left at double the distance.
- **Alt-Shift-S**: Move the mouse cursor down at double the distance.
- **Alt-Shift-D**: Move the mouse cursor right at double the distance.
- **Alt-Control-W**: Move the mouse cursor up at half the distance.
- **Alt-Control-A**: Move the mouse cursor left at half the distance.
- **Alt-Control-S**: Move the mouse cursor left at half the distance.
- **Alt-Control-D**: Move the mouse cursor left at half the distance.
- **Alt-1**: Move the mouse cursor to the top left of the screen.
- **Alt-2**: Move the mouse cursor to the top center of the screen.
- **Alt-3**: Move the mouse cursor to the top right of the screen.
- **Alt-4**: Move the mouse cursor to the middle left of the screen.
- **Alt-5**: Move the mouse cursor to the center of the screen.
- **Alt-6**: Move the mouse cursor to the middle right of the screen.
- **Alt-7**: Move the mouse cursor to the bottom left of the screen.
- **Alt-8**: Move the mouse cursor to the bottom center of the screen.
- **Alt-9**: Move the mouse cursor to the bottom right of the screen.
- **Alt-0**: Move the mouse cursor to the center of the current open window.
- **Alt-Shift-1**: Snaps the current window to the upper-left screen quadrant.
- **Alt-Shift-2**: Snaps the current window to the lower-left screen quadrant.
- **Alt-Shift-3**: Snaps the current window to the upper-right screen quadrant.
- **Alt-Shift-4**: Snaps the current window to the lower-right screen quadrant.
- **Alt-Shift-Backslash**: Minimize the current window.
- **Alt-Shift-0**: Maximize the current window.
- **Alt-Shift-Left Curly Brace**: Snap the current window to the left.
- **Alt-Shift-Right Curly Brace**: Snap the current window to the right.