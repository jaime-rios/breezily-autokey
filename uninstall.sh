autokey_path=~/.config/autokey
autokey_data_path=~/.config/autokey/data
autokey_scripts_path=~/.config/autokey/data/autokey-hyper-scripts
autokey_hyper_scripts=./autokey-hyper-scripts

remove_autokey_scripts() {

  sleep 2s
  echo "\nChecking if '$autokey_path' directory exists..."
  sleep 2s

  if [ -d $autokey_path ] ; then

    echo "> '$autokey_path' was found! Examining..."
    sleep 2s
    echo "\nNow checking if '$autokey_data_path' directory exists..."
    sleep 2s

    if [ -d $autokey_data_path ] ; then

      echo "> '$autokey_data_path' was found!"
      sleep 1s
      echo "> Seeing if the Breezily Autokey scripts exist..."
      sleep 2s

      if [ -d $autokey_scripts_path ]; then

        echo "> The Autokey scripts were found!"
        sleep 1s
        echo "> Deleting these scripts now..."
        rm -rf autokey_scripts_path -v
        sleep 2s
        echo "> Done! Moving on..."

      else

        echo "> The Autokey scripts have been removed already."
        sleep 2s
        echo "> Moving on..."
      fi

    else
      echo "> '$autokey_data_path' was not found..."
      sleep 2s
      echo "> The Autokey scripts have been removed already."
      sleep 2s
      echo "> Moving on!"
    fi

  else
    echo "> '$autokey_path' directory was not found..."
    sleep 2s
    echo "> The Autokey scripts have been removed already."
    sleep 1s
    echo "> Moving on!"
  fi

}

xkb_symbols_path=/usr/share/X11/xkb/symbols
xkb_symbols_backup=~/.xkb-symbols-backup

restore_caps_lock_key() {

  sleep 2s
  echo "Now restoring the Caps_Lock to its original state."
  sleep 2s
  echo "\nFirst, checking if the '$xkb_symbols_backup' file exists."
  sleep 2s

  if [ -f $xkb_symbols_backup ]; then

    echo "> '$xkb_symbols_backup' file was found! Restoring now..."
    sleep 2s
    cp $xkb_symbols_backup $xkb_symbols_path -v
    sleep 2s
    echo "> Done! The Caps_Lock key was restored to its original state."
    sleep 2s
    echo "> Moving on!"

  else
    echo "> Whoops! The '$xkb_symbols_backup' file was not found..."
    sleep 2s
    echo "\nThe Caps_Lock was not restored to its original state."
  fi

}

uninstall_xbindkeys_xdotool() {
  sudo apt remove xbindkeys xdotool
  wait
}

remove_xbindkeysrc_file() {
  sleep 2s
  echo "\nNow deleting the '.xbindkeysrc' file from your home '~/' directory."
  rm ./xbindkeysrc -v
  sleep 2s
  echo "Done!"
}

ending_msg() {
  sleep 2s
  echo "Make sure to logout and log back in for the changes to take effect."
  sleep 2s
  echo "The uninstall script has completed."
}

exit_cmd() {
  sleep 2s
  echo "Now exiting..."
  sleep 1s
  exit
}

echo "\nWelcome to the Breezily uninstall script!"
echo -n "\nDo you want to continue? [y,N]\n"
read answer

if [ "$answer" != "${answer#[Yy]}" ]; then
  echo "\nOkay! Here are your uninstall options..."
  sleep 2s

  echo "\n- Option 1"
  echo "  > Restore the xkb symbols file to its default settings (restores the Caps_Lock key to original state)."
  echo "  > Remove all Breezily Autokey hyper scripts."

  echo "\n- Option 2"
  echo "  > Remove both 'xbindkeys' and 'xdotool' packages."
  echo "  > Delte the .xbindkeysrc file from your home '~/' directory."

  echo "\n3) Option 3: Full Uninstall"
  echo "  - Includes both Options 1 and 2."

  echo "\n0) Exit install script."

  echo "\nPlease enter in your option below:"
  read option
  if [ "$option" = "1" ]; then
    echo "\nYou chose option $option..."
    remove_autokey_scripts
    restore_caps_lock_key
    ending_msg
    exit_cmd

  elif [ "$option" = "2" ]; then
    uninstall_xbindkeys_xdotool
    remove_xbindkeysrc_file
    ending_msg
    exit_cmd

  elif [ "$option" = "3" ]; then
    echo "\nYou chose option $option..."
    remove_autokey_scripts
    restore_caps_lock_key
    uninstall_xbindkeys_xdotool
    remove_xbindkeysrc_file
    ending_msg
    exit_cmd

  else
    exit_cmd
  fi

else
  exit_cmd
fi